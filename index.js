// alert("Welcome to the site!")


const servicePerElement = document.querySelector('.service-per');

const observer = new IntersectionObserver(entries => {
    entries.forEach(entry => {
        if (entry.isIntersecting) {
            servicePerElement.classList.add('service-per-animate');
            observer.unobserve(entry.target);
        }
    });
});

observer.observe(servicePerElement);

const withdrawPerElement = document.querySelector('.withdrawal-per');
const observer2 = new IntersectionObserver(entries => {
    entries.forEach(entry => {
        if (entry.isIntersecting) {
            withdrawPerElement.classList.add('withdrawal-per-animate');
            observer2.unobserve(entry.target);
        }
    });
});

observer2.observe(withdrawPerElement);


